﻿using System;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DarwinSw.BasicAuthenticationExtension {
    public class DarwinBasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions> {
        private readonly UserAuthenticationService userAuthenticationService;

        public DarwinBasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            UserAuthenticationService userAuthenticationService)
            : base(options, logger, encoder, clock) {
            this.userAuthenticationService = userAuthenticationService;
        }

        private AuthenticateResult CreateError(string message) {
            Response.Headers.Add("X-Authentication-Error", message);
            return AuthenticateResult.Fail(message);
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync() {
            // skip authentication if endpoint has [AllowAnonymous] attribute
            var endpoint = Context.GetEndpoint();
            if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null)
                return AuthenticateResult.NoResult();

            if (!Request.Headers.ContainsKey("Authorization")) {
                return CreateError("Missing Authorization Header");
            }

            BasicAuthenticatedUser user = null;
            try {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(new[] { ':' }, 2);
                var username = credentials[0];
                var password = credentials[1];
                user = await userAuthenticationService.Authenticate(username, password);
            } catch {
                return CreateError("Invalid Authorization Header");
            }

            if (user == null)
                return CreateError("Invalid Username or Password");

            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, user.Name.ToString()),
                new Claim(ClaimTypes.Name, user.Name),
            };
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            return AuthenticateResult.Success(ticket);
        }
    }
}