using System.Threading.Tasks;

namespace DarwinSw.BasicAuthenticationExtension {
    public class FakeUserAuthenticationService : UserAuthenticationService {
        public Task<BasicAuthenticatedUser> Authenticate(string username, string password) {
            if (username == "userName" && password == "password") {
                return Task.FromResult(new BasicAuthenticatedUser("Basic", true, username));
            }

            return Task.FromResult<BasicAuthenticatedUser>(null);
        }
    }
}