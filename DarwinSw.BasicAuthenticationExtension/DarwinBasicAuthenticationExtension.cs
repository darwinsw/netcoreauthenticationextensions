using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;

namespace DarwinSw.BasicAuthenticationExtension {
    public static class DarwinBasicAuthenticationExtension {
        public static void UseBasicAuthentication<TAuthService>(this IServiceCollection services) where TAuthService : class, UserAuthenticationService {
            services.AddAuthentication(opt => {
                    opt.DefaultAuthenticateScheme = "BasicAuthentication";
                    opt.DefaultChallengeScheme = "BasicAuthentication";
                    opt.RequireAuthenticatedSignIn = true;
                })
                .AddScheme<AuthenticationSchemeOptions, DarwinBasicAuthenticationHandler>("BasicAuthentication",
                    options => {
                    });
            services.AddSingleton<UserAuthenticationService, TAuthService>();
            services.AddScoped<DarwinBasicAuthenticationHandler>();
            
        }
    }
}