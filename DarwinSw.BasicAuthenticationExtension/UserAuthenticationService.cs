using System.Threading.Tasks;

namespace DarwinSw.BasicAuthenticationExtension {
    public interface UserAuthenticationService {
        Task<BasicAuthenticatedUser> Authenticate(string username, string password);
    }
}