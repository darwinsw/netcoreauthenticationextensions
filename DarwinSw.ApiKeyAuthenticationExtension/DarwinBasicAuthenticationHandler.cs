﻿using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DarwinSw.ApiKeyAuthenticationExtension {
    public class DarwinApiKeyAuthenticationHandler : AuthenticationHandler<ApiKeyAuthenticationSchemeOptions> {
        private readonly ApiKeyAuthenticationService apiKeyAuthenticationService;

        public DarwinApiKeyAuthenticationHandler(
            IOptionsMonitor<ApiKeyAuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            ApiKeyAuthenticationService apiKeyAuthenticationService)
            : base(options, logger, encoder, clock) {
            this.apiKeyAuthenticationService = apiKeyAuthenticationService;
        }

        private AuthenticateResult CreateError(string message) {
            Response.Headers.Add("X-Authentication-Error", message);
            return AuthenticateResult.Fail(message);
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync() {
            // skip authentication if endpoint has [AllowAnonymous] attribute
            var endpoint = Context.GetEndpoint();
            if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null)
                return AuthenticateResult.NoResult();

            if (!Request.Headers.ContainsKey(Options.ApiKeyHeader)) {
                return CreateError($"Missing {Options.ApiKeyHeader} Header");
            }

            var apiKey = Request.Headers[Options.ApiKeyHeader];
            var user = await apiKeyAuthenticationService.Authenticate(apiKey);

            if (user == null)
                return CreateError($"Invalid ApiKey in {Options.ApiKeyHeader} Header");

            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, user.Name.ToString()),
                new Claim(ClaimTypes.Name, user.Name),
            };
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            return AuthenticateResult.Success(ticket);
        }
    }
}