using Microsoft.AspNetCore.Authentication;

namespace DarwinSw.ApiKeyAuthenticationExtension {
    public class ApiKeyAuthenticationSchemeOptions : AuthenticationSchemeOptions {
        public string ApiKeyHeader { get; set; } = "X-ApiKey";
    }
}