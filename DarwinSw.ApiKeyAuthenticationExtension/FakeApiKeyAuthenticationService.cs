using System.Threading.Tasks;

namespace DarwinSw.ApiKeyAuthenticationExtension {
    public class FakeApiKeyAuthenticationService : ApiKeyAuthenticationService {
        public Task<ApiKeyAuthenticatedUser> Authenticate(string apiKey) {
            if (apiKey == "abc123") {
                return Task.FromResult(new ApiKeyAuthenticatedUser("Basic", true, "fake-user"));
            }

            return Task.FromResult<ApiKeyAuthenticatedUser>(null);
        }
    }
}