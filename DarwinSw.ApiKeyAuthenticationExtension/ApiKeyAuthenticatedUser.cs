using System.Security.Principal;

namespace DarwinSw.ApiKeyAuthenticationExtension {
    public class ApiKeyAuthenticatedUser : IIdentity {
        public ApiKeyAuthenticatedUser(string authenticationType, bool isAuthenticated, string name) {
            AuthenticationType = authenticationType;
            IsAuthenticated = isAuthenticated;
            Name = name;
        }

        public string AuthenticationType { get; }

        public bool IsAuthenticated { get; }

        public string Name { get; }
    }
}