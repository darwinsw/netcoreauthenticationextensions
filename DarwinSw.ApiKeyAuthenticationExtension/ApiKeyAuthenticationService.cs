using System.Threading.Tasks;

namespace DarwinSw.ApiKeyAuthenticationExtension {
    public interface ApiKeyAuthenticationService {
        Task<ApiKeyAuthenticatedUser> Authenticate(string apiKey);
    }
}