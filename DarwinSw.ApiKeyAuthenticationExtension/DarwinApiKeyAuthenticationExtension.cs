using System;
using Microsoft.Extensions.DependencyInjection;

namespace DarwinSw.ApiKeyAuthenticationExtension {
    public static class DarwinApiKeyAuthenticationExtension {
        public static void UseApiKeyAuthentication<TAuthService>(this IServiceCollection services,
            Action<ApiKeyAuthenticationSchemeOptions> configure = null)
            where TAuthService : class, ApiKeyAuthenticationService {
            services.AddAuthentication(opt => {
                    opt.DefaultAuthenticateScheme = "ApiKeyAuthentication";
                    opt.DefaultChallengeScheme = "ApiKeyAuthentication";
                    opt.RequireAuthenticatedSignIn = true;
                })
                .AddScheme<ApiKeyAuthenticationSchemeOptions, DarwinApiKeyAuthenticationHandler>("ApiKeyAuthentication",
                    configure ??
                    (options => { }));
            services.AddSingleton<ApiKeyAuthenticationService, TAuthService>();
            services.AddScoped<DarwinApiKeyAuthenticationHandler>();
        }
    }
}